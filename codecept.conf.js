let config = {
  tests: "./*_test.js",
  output: "./output",
  helpers: {
    WebDriver: {
      url: "http://the-internet.herokuapp.com",
      browser: "chrome",
      windowSize: "1920x1080"
    },
    REST: {}
  },
  include: {
    I: "./steps_file.js"
  },
  bootstrap: null,
  mocha: {},
  name: "my-auto-e2e-tests"
};

if (process.profile === "chrome-ci") {
  config.helpers.WebDriver.host =
    process.env.SELENIUM_STANDALONE_CHROME_PORT_4444_TCP_ADDR;
  // Reset config because it got overriden by BrowserStack auto detect mechanism
  config.helpers.WebDriver.protocol = "http";
  config.helpers.WebDriver.port = 4444;
}

exports.config = config;
